from datetime import datetime


class Vehicle(object):
    def __init__(self, brand, model, mileage, last_service):
        self.brand = brand
        self.model = model
        self.mileage = mileage
        self.brand = brand
        self.last_service = last_service

    def print_common(self, i):
        print '%s) %s %s' % (i, self.brand, self.model)
        print 'MILEAGE: %s km' % self.mileage
        print 'LAST SERVICE:', self.last_service


class Car(Vehicle):
    def __init__(self, brand, model, mileage, last_service, color):
        Vehicle.__init__(self, brand, model, mileage, last_service)
        self.color = color


class Truck(Vehicle):
    def __init__(self, brand, model, mileage, last_service, capacity):
        Vehicle.__init__(self, brand, model, mileage, last_service)
        self.capacity = capacity


class Boat(Vehicle):
    def __init__(self, brand, model, mileage, last_service, length):
        Vehicle.__init__(self, brand, model, mileage, last_service)
        self.length = length


def show_list(catalogue):
    print '\nCARS:\n'
    for i, vehicle in enumerate(catalogue[0][1:], start=1):
        vehicle.print_common(i)
        print 'COLOR:', vehicle.color
        print''
    print '\nTRUCKS:\n'
    for i, vehicle in enumerate(catalogue[1][1:], start=1):
        vehicle.print_common(i)
        print 'CAPACITY: %s KG' % vehicle.capacity
        print''
    print '\nBOATS:\n'
    for i, vehicle in enumerate(catalogue[2][1:], start=1):
        vehicle.print_common(i)
        print 'LENGTH: %s m' % vehicle.length
        print''


def edit_mileage(catalogue):
    print "Enter the ID of the vehicle group:"
    for index, group in enumerate(catalogue, start=1):
        print index, group[0]

    while True:
        while True:
            try:
                selected_group_id = int(raw_input("Enter ID:"))
                break
            except ValueError:
                print "Oops! You didn't enter a valid number. Try again!"
        if 0 < selected_group_id < (len(catalogue) + 1):
            break
        else:
            print "Oops! You didn't enter a valid number. Try again!"

    selected_group = catalogue[int(selected_group_id) - 1]

    print "Enter the ID of the vehicle you wish to edit mileage for:"

    for index, vehicle in enumerate(selected_group[1:], start=1):
        print str(index) + ') ' + vehicle.brand + ' ' + vehicle.model

    print ''

    while True:
        while True:
            try:
                selected_id = int(raw_input("Enter ID:"))
                break
            except ValueError:
                print "Oops! You didn't enter a valid number. Try again!"
        if 0 < selected_id < len(selected_group):
            break
        else:
            print "Oops! You didn't enter a valid number. Try again!"

    selected_vehicle = selected_group[int(selected_id)]

    print "Current mileage of " + selected_vehicle.brand + ' ' + selected_vehicle.model + \
          ': ' + str(selected_vehicle.mileage)
    selected_vehicle.mileage = int(raw_input("Enter new mileage:"))
    print ''
    print "Mileage for " + selected_vehicle.brand + " " + selected_vehicle.model + \
          " with ID: " + str(selected_id) + " successfully updated!"


def change_service_date(catalogue):
    print "Enter the ID of the vehicle group:"
    for index, group in enumerate(catalogue, start=1):
        print index, group[0]

    while True:
        while True:
            try:
                selected_group_id = int(raw_input("Enter ID:"))
                break
            except ValueError:
                print "Oops! You didn't enter a valid number. Try again!"
        if 0 < selected_group_id < (len(catalogue) + 1):
            break
        else:
            print "Oops! You didn't enter a valid number. Try again!"

    selected_group = catalogue[int(selected_group_id) - 1]

    print "Enter the ID of the vehicle you wish to edit service date for:"

    for index, vehicle in enumerate(selected_group[1:], start=1):
        print str(index) + ') ' + vehicle.brand + ' ' + vehicle.model

    print ''

    while True:
        while True:
            try:
                selected_id = int(raw_input("Enter ID:"))
                break
            except ValueError:
                print "Oops! You didn't enter a valid number. Try again!"
        if 0 < selected_id < len(selected_group):
            break
        else:
            print "Oops! You didn't enter a valid number. Try again!"

    selected_vehicle = selected_group[int(selected_id)]

    print "Date of last service:", selected_vehicle.last_service
    selected_vehicle.last_service = raw_input("Enter new date:")
    print "Date of service successfully edited!"


def add_new(catalogue):
    while True:
        while True:
            try:
                group = int(raw_input("Enter vehicle group (1=car, 2=truck, 3=boat):"))
                break
            except ValueError:
                print "Oops! You didn't enter a valid number. Try again!"
        if 0 < group < 4:
            break
        else:
            print "Oops! You didn't enter a valid number. Try again!"

    brand = raw_input("Enter brand:")
    model = raw_input("Enter model:")
    mileage = int(raw_input("Enter mileage:"))
    last_service = raw_input("Enter last service:")

    while True:
        if group == 1:
            color = raw_input("Enter color:")
            new = Car(brand, model, mileage, last_service, color)
            catalogue[0].append(new)
            print "New car added successfully!"
            break
        elif group == 2:
            capacity = raw_input("Enter capacity:")
            new = Truck(brand, model, mileage, last_service, capacity)
            catalogue[1].append(new)
            print "New truck added successfully!"
            break
        elif group == 3:
            length = raw_input("Enter length:")
            new = Boat(brand, model, mileage, last_service, length)
            catalogue[2].append(new)
            print "New boat added successfully!"
            break


def delete_vehicle(catalogue):

    while True:
        while True:
            try:
                selected_group_id = int(raw_input("Enter vehicle group (1=cars, 2=trucks, 3=boats):"))
                break
            except ValueError:
                print "Oops! You didn't enter a valid number. Try again!"
        if 0 < selected_group_id < 4:
            break
        else:
            print "Oops! You didn't enter a valid number. Try again!"

    group = catalogue[int(selected_group_id) - 1]

    print "Enter the ID of the vehicle you wish to delete:"

    for index, vehicle in enumerate(group[1:], start=1):
        print str(index) + ') ' + vehicle.brand + ' ' + vehicle.model

    print ''

    while True:
        while True:
            try:
                selected_id = int(raw_input("Enter ID:"))
                break
            except ValueError:
                print "Oops! You didn't enter a valid number. Try again!"
        if 0 < selected_id < len(group):
            break
        else:
            print "Oops! You didn't enter a valid number. Try again!"

    selected_vehicle = group[int(selected_id)]

    yn = raw_input("Are you sure you wish to delete %s %s with the ID: %s? (y/n)" % (selected_vehicle.brand, selected_vehicle.model, selected_id))
    if yn == 'y':
        group.remove(selected_vehicle)
        print "Vehicle deleted successfully."
    else:
        print "Deletion aborted."


def save(catalogue):

    if raw_input('Are you sure? (y/n)').lower() == 'y':

        with open('save.txt', 'w') as car_park_file:
            now = datetime.now()
            car_park_file.write('CAR PARK CATALOGUE \n')
            car_park_file.write('Last edit: %s/%s/%s, %s:%s \n\n'
                                % (now.strftime('%b'), now.day, now.year, now.hour, now.minute))

            for group in catalogue:
                car_park_file.write(str(group[0]) + '\n')
                for index, vehicle in enumerate(group[1:]):
                    car_park_file.write('ID: %s \n' % index)
                    car_park_file.write('%s %s \n' % (vehicle.brand, vehicle.model))
                    car_park_file.write('MILEAGE: %s \n' % vehicle.mileage)
                    car_park_file.write('LAST SERVICE: %s \n\n' % vehicle.last_service)

        print "Catalogue saved successfully.\n"

    else:
        print 'Save to file canceled.'


def main():
    print "Welcome to Vehicle manager!"

    polo = Car('VW', 'Polo', 150, '2/2/2015', 'blue')
    tiguan = Car('VW', 'Tiguan', 25, '2/2/2018', 'white')
    volvo = Truck('Volvo', 'FM', 120, '2/5/2010', 2000)
    mercedes = Truck('Mercedes', 'XX', 123, '2/5/2000', 2200)
    boat = Boat('Boats inc.', 'Model B', 150, '12/6/2017', 65)

    cars = ['Cars', polo, tiguan]
    trucks = ['Trucks', volvo, mercedes]
    boats = ['Boats', boat]

    vehicles = [cars, trucks, boats]

    while True:
        print "Please choose one of these options:"
        print "1) View catalogue"
        print "2) Edit mileage"
        print "3) Edit date of last service"
        print "4) Add a new vehicle"
        print "5) Delete a vehicle"
        print "6) Save catalogue to file"
        print ""

        selection = raw_input("Enter your selection or enter 'q' to quit:")

        if selection.lower() == "1":
            show_list(vehicles)
        elif selection.lower() == "2":
            edit_mileage(vehicles)
        elif selection.lower() == "3":
            change_service_date(vehicles)
        elif selection.lower() == "4":
            add_new(vehicles)
        elif selection.lower() == "5":
            delete_vehicle(vehicles)
        elif selection.lower() == "6":
            save(vehicles)
        elif selection.lower() == "q":
            print "Goodbye!"
            break
        else:
            print "Your selection didn't match any of the options. Please try again."


if __name__ == "__main__":
    main()
